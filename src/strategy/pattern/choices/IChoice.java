package strategy.pattern.choices;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public interface IChoice {
    void myChoice(String s1, String s2);
}