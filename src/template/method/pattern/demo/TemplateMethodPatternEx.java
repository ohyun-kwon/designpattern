package template.method.pattern.demo;

import template.method.pattern.engineering.papers.BasicEngineering;
import template.method.pattern.engineering.papers.ComputerScience;
import template.method.pattern.engineering.papers.Electronics;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
class TemplateMethodPatternEx {
    public static void main(String[] args) {
        System.out.println("***Template Method Pattern Demo***\n");
        BasicEngineering bs = new ComputerScience();
        System.out.println("Computer Sc Papers:");
        bs.Papers();
        System.out.println();
        bs = new Electronics();
        System.out.println("Electronics Papers:");
        bs.Papers();
    }
}
