package template.method.pattern.engineering.papers;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public class Electronics extends BasicEngineering {
    @Override
    public void SpecialPaper() {
        System.out.println("Digital Logic and Circuit Theory");
    }
}
