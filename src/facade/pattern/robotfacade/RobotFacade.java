package facade.pattern.robotfacade;

import facade.pattern.robotparts.RobotBody;
import facade.pattern.robotparts.RobotColor;
import facade.pattern.robotparts.RobotMetal;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public class RobotFacade {
    RobotColor rc;
    RobotMetal rm;
    RobotBody rb;

    public RobotFacade() {
        rc = new RobotColor();
        rm = new RobotMetal();
        rb = new RobotBody();
    }

    public void ConstructRobot(String color, String metal) {
        System.out.println("\nCreation of the Robot Start");
        rc.SetColor(color);
        rm.SetMetal(metal);
        rb.CreateBody();
        System.out.println(" \nRobot Creation End");
        System.out.println();
    }
}
