package facade.pattern.robotparts;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public class RobotColor {
    private String color;

    public void SetColor(String color) {
        this.color = color;
        System.out.println("Color is set to : " + this.color);
    }
}
