package facade.pattern.robotparts;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public class RobotBody {
    public void CreateBody() {
        System.out.println("Body Creation done");
    }
}
