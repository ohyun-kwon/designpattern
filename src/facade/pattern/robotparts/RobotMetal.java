package facade.pattern.robotparts;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public class RobotMetal {
    private String metal;

    public void SetMetal(String metal) {
        this.metal = metal;
        System.out.println("Metal is set to : " + this.metal);
    }
}
