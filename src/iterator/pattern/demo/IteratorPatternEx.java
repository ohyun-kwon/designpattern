package iterator.pattern.demo;

import iterator.pattern.aggregate.Arts;
import iterator.pattern.aggregate.ISubject;
import iterator.pattern.aggregate.Science;
import iterator.pattern.iterator.IIterator;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
class IteratorPatternEx {
    public static void main(String[] args) {
        System.out.println("***Iterator Pattern Demo***\n");
        ISubject Sc_subject = new Science();
        ISubject Ar_subjects = new Arts();
        IIterator Sc_iterator = Sc_subject.CreateIterator();
        IIterator Ar_iterator = Ar_subjects.CreateIterator();
        System.out.println("\nScience subjects :");
        Print(Sc_iterator);
        System.out.println("\nArts subjects :");
        Print(Ar_iterator);
    }

    public static void Print(IIterator iterator) {
        while (!iterator.IsDone()) {
            System.out.println(iterator.Next());
        }
    }
}