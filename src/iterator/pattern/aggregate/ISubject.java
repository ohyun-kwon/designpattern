package iterator.pattern.aggregate;

import iterator.pattern.iterator.IIterator;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public interface ISubject {
    public IIterator CreateIterator();
}
