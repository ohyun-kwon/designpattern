package proxy.pattern.OriginalClasses;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public abstract class Subject {
    public abstract void doSomeWork();
}