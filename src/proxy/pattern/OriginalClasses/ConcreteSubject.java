package proxy.pattern.OriginalClasses;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public class ConcreteSubject extends Subject
{
    @Override
    public void doSomeWork()
    {
        System.out.println(" I am from concrete subject");
    }
}
