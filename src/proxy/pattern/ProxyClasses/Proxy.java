package proxy.pattern.ProxyClasses;

import proxy.pattern.OriginalClasses.ConcreteSubject;
import proxy.pattern.OriginalClasses.Subject;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
public class Proxy extends Subject {
    ConcreteSubject cs;

    @Override
    public void doSomeWork() {
        System.out.println("Proxy call happening now");
//Lazy initialization
        if (cs == null) {
            cs = new ConcreteSubject();
        }
        cs.doSomeWork();
    }
}