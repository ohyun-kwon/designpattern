package proxy.pattern.demo;

import proxy.pattern.ProxyClasses.Proxy;

/**
 * Created by iloveuu on 2017. 1. 17..
 */
class ProxyPatternEx
{
    public static void main(String[] args)
    {
        System.out.println("***Proxy Pattern Demo***\n");
        Proxy px = new Proxy();
        px.doSomeWork();
    }
}