package prototype.pattern.car;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
public class Nano extends BasicCar {
    public Nano(String m) {
        modelname = m;
    }

    @Override
    public BasicCar clone() throws CloneNotSupportedException {
        return (Nano) super.clone();
    }
}
