package prototype.pattern.car;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
public class Ford extends BasicCar {
    public Ford(String m) {
        modelname = m;
    }

    @Override
    public BasicCar clone() throws CloneNotSupportedException {
        return (Ford) super.clone();
    }
}

